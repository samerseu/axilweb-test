<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CallLog;

class TestController extends Controller
{		
    public function vueTest()  {
		return view('sample-vue-with-cdn');
	}
	
		
	
	
	
	/*************************
		api for call logs
	**************************/
	public function callLog() {
		$callLogs = CallLog::select([
			'id',
			'call_date', 
			'phone_number' , 
			'call_duration', 
			'status'			
		])
		->orderBy('id', 'desc')
		->get();
				
		return response()
				->json($callLogs);
	}
	
	/******************************
		api for date vs no of call
	********************************/
	public function dateVsNoOfCall() {
		$callLogs = CallLog::select([
			'call_date', 
			\DB::raw('count(*) as no_of_call')			
		])
		->groupBy('call_date')
		->get();		
		
		return response()
				->json($callLogs);
	}
	
}
