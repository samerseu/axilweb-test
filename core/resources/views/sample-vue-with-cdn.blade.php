<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    </head>
    <body>        
        <div id="app">             
		{{ message }}
			<div class="row">
				<div class="col-md-6">
					<select class="form-control" v-model="selectedStatus" v-on:change="statusChange">			
						<option v-for="s in statusList"
							v-bind:value="s.name"
							>{{ s.name }}</option>
					</select>
				</div>	 
				<div class="col-md-6">
					<input class="form-control" />
				</div>
			</div>	 
			
			<table class="table" width="100%" cellpadding="5" cellspacing="5">
			<thead>
				<tr>
					<td>Call Date</td>
					<td>Phone Number</td>
					<td>Call Duration</td>
					<td>Status</td>
				</tr>
			</thead>
			<tbody>
				<tr v-for="d in dataList">
					<td>{{ d.call_date }}</td>
					<td>{{ d.phone_number }}</td>
					<td>{{ d.call_duration }}</td>
					<td>{{ d.status }}</td>
				</tr>
			</tbody>
			</table>
			
        </div>
        
        <script type="text/javascript">
		var list = [
						{
						  "id": 1, 
						  "call_date": "2020-06-19",
						  "phone_number": "01821981923",
						  "call_duration": "55",
						  "status": "In-call"	
						},
						{
						  "id": 2, 
						  "call_date": "2020-06-18",
						  "phone_number": "01821981923",
						  "call_duration": "58",
						  "status": "call back"	
						},
						{
						  "id": 3, 
						  "call_date": "2020-06-19",
						  "phone_number": "01821981923",
						  "call_duration": "55",
						  "status": "hold"	
						},
						{
						  "id": 4, 
						  "call_date": "2020-06-18",
						  "phone_number": "01821981923",
						  "call_duration": "58",
						  "status": "call back"	
						}
					];
					
		var statusList = [
			{id: 1, name: "In-call"},
			{id: 2, name: "hold"},
			{id: 3, name: "call-back"},
			{id: 4, name: "do not call"}
		];
		var app = new Vue({ 
			el: '#app',
			data: {
				message: 'Hello Vue!',				
				selectedStatus: null,
				dataList: list ,
				statusList: statusList
			},
			methods: {
				statusChange: (e) => {
					console.log(e.target.options.selectedIndex);					
					if(e.target.options.selectedIndex > -1) {
						console.log(statusList[e.target.options.selectedIndex].name);						
					}
				}
			}
		});
		
		//https://stackoverflow.com/questions/48201600/vue-js-how-to-filter-a-table-with-a-custom-filter
		//https://apexcharts.com/vue-chart-demos/timeline-charts/basic/
		
		</script>
    </body>
</html>